import axios from '../axios-counter';

export const REQUEST = 'REQUEST';
export const CHANGE_TASK_TEXT = 'CHANGE_TASK_TEXT';
export const GET_TASKS = 'GET_TASKS';
export const ERROR_RESPONSE = 'ERROR_RESPONSE';


export const request = () => {
  return {type: REQUEST};
};

export const changeTaskText = text => {
  return {type: CHANGE_TASK_TEXT, text};
};

export const sendNewTask = text => dispatch => {
  dispatch(request());
  axios.post('/tasks.json', {text, checkbox: false})
    .then(() => dispatch(requestTasks()));
};

export const requestTasks = () => dispatch => {
  dispatch(request());
  axios.get('/tasks.json')
    .then(response => response ? dispatch(getTasks(response.data)) : null);
};

export const getTasks = tasks => {
  return {type: GET_TASKS, tasks};
};

export const RequestRemove = key => dispatch => {
  dispatch(request());
  axios.delete(`/tasks/${key}.json`)
    .then(response => response ? dispatch(requestTasks()) : null);
};

export const RequestCheckbox = key => (dispatch, getState) => {
  dispatch(request());
  axios.patch(`/tasks/${key}.json`, {checkbox: !getState().tasks[key].checkbox})
    .then(response => response ? dispatch(requestTasks()) : null)
    .catch(() => dispatch(ErrorResponse()));
};

export const ErrorResponse = () => {
  return {type: ERROR_RESPONSE};
};













// export const incrementSuccess = counter => {
//   return {type: INCREMENT_SUCCESS, counter};
// };
//
// export const incrementError = () => {
//   return {type: INCREMENT_ERROR};
// };
//
//
// export const fetchCounter = () => {
//   // цель отправки функций через dispatch - это в итоге получение JS объекта
//   // который будет отправлен в reduser
//   // Redux не понимает функции, а понимает только JS объекты и когда функция
//   // отправляется в redux - thunk ее перехватывает и выполняется thunk(ом)
//   // в результате которой в redux должен отправиться понятный ему action.
//   // thunk это перехватчик как interceptor чтобы redux понял что
//   // отправляется функция.
//   return dispatch => {
//     dispatch(request());
//     axios.get('/counter.json')
//       .then(response => dispatch(fetchCounterSuccess(response.data)))
//       .catch(() => dispatch(fetchCounterError()));
//   }
// };
//
// export const fetchCounterSuccess = (counter) => {
//   return {type: FETCH_COUNTER_SUCCESS, counter};
// };
//
// export const fetchCounterError = () => {
//   return {type: FETCH_COUNTER_ERROR};
// };



// import {} from './actions';

import {CHANGE_TASK_TEXT, ERROR_RESPONSE, GET_TASKS, REQUEST} from "./actions";

const initialState = {
  tasks: {},
  textOfTask: '',
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST:
      return {...state, loading: true};
    case CHANGE_TASK_TEXT:
      return {...state, textOfTask: action.text};
    case GET_TASKS:
      return {...state, tasks: action.tasks, loading: false};
    case ERROR_RESPONSE:
      return {...state, loading: false};
    default:
      return state;
  }
};

export default reducer;
import React, {Component} from 'react';
import './AddTaskForm.css';
import {changeTaskText, sendNewTask} from "../../store/actions";
import {connect} from "react-redux";

class AddTaskForm extends Component {

  changeInput = event => {
    this.props.changeTaskText(event.target.value)
  };

  render() {
    return (
      <div className='AddTaskForm'>
          <input className='AddTask' type="text"
                 onChange={this.changeInput}
                 value={this.props.textOfTask}/>
          <button className='AddBtn'
                  onClick={() => this.props.sendNewTask(this.props.textOfTask)}>
            Add task</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    textOfTask: state.textOfTask,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    changeTaskText: (text) => dispatch(changeTaskText(text)),
    sendNewTask: (text) => dispatch(sendNewTask(text)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTaskForm);
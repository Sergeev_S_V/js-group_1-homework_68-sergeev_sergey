import React, {Component} from 'react';
import './Tasks.css';
import {connect} from "react-redux";
import {RequestCheckbox, RequestRemove, requestTasks} from "../../store/actions";
import Task from "./Task/Task";
import Spinner from "../../hoc/Spinner/Spinner";

class Tasks extends Component {

  componentDidMount() {
    this.props.requestTasks();
  }

  render() {
    if (!this.props.loading && this.props.tasks) {
      return(
        <div className='tasks'>
          {Object.keys(this.props.tasks).map(key => {
            return <Task
              key={key}
              text={this.props.tasks[key].text}
              remove={() => this.props.remove(key)}
              checkbox={() => this.props.checkbox(key)}
              done={this.props.tasks[key].checkbox}/>
          })}
        </div>
      );
    } else if (this.props.loading) {
      return <Spinner/>
    } else {
      return <div>Tasks list is empty</div>
    }
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.tasks,
    loading: state.loading,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    requestTasks: () => dispatch(requestTasks()),
    remove: (key) => dispatch(RequestRemove(key)),
    checkbox: (key) => dispatch(RequestCheckbox(key)),
  }
};



export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <div className='Task'>
            <span onClick={props.remove}>X</span>
            {props.done ? <p className='Done'>Задание выполнено</p> : <p className='Done'>Не выполнено</p>}
            <p className='Text'>{props.text}</p>
            <div><button onClick={props.checkbox}>Task complete</button></div>
        </div>
    );
};

export default Task;
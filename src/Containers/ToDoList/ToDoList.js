import React, { Component } from 'react';
import AddTaskForm from "../../Components/AddTaskForm/AddTaskForm";
import Tasks from "../../Components/Tasks/Tasks";

class ToDoList extends Component {
  render() {
    return (
      <div className="ToDoList">
        <AddTaskForm/>
        <Tasks/>
      </div>
    );
  }
}

export default ToDoList;
